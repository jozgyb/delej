# Delej összesség

## Kézi szerszámok

### Csavarhúzók

- Yato SL+PH+PZ szigetelt csavarhúzó szett [YT-2828](https://www.szerszamoutlet.hu/index.php?_yato_csavarhuzo_keszlet_7_reszes_1000vig_szigetelt_slphpz_yt2828&kat=254&id=62989)

- Milwaukee [PZ/SL2 csavarhúzó](https://www.szerszamoutlet.hu/index.php?_milwaukee_vde_szigetelt_csavarhuzo_szerelvenyezo_pzsl_2x100mm_4932464058_4932478736&kat=629&id=76867) (lakáselosztóhoz, fogyasztásmérőhöz kell leginkább)

- Milwaukee [PZ/SL1 csavarhúzó](https://www.szerszamoutlet.hu/index.php?_milwaukee_vde_szigetelt_csavarhuzo_szerelvenyezo_pzsl_1x80mm_4932464057_4932478735&kat=629&id=76866) (szerelvényezéshez)

- (opcionális) Lidl-ben van Parkside vésőcsavarhúzó szett püfölhető meg praktikus mindenféle destrukcióhoz

### Fogók

- Yato oldalcsípő 160mm VDE [YT-21158](https://www.szerszamoutlet.hu/index.php?_yato_oldalcsipo_fogo_1000_vig_szigetelt_160mm_yt21158&kat=368&id=62895)

- Yato kábelvágó [YT-1966](https://www.szerszamoutlet.hu/index.php?_yato_kabelvago_fogo_160mm_yt1966&kat=642&id=12004) (nem szigetelt, mert ahhoz egy fél vesét el kell adni)

- Yato kombinált fogó 180mm VDE [YT-21152](https://www.szerszamoutlet.hu/index.php?_yato_kombinalt_fogo_1000vig_szigetelt_180mm_yt21152&kat=367&id=62891)

- Yato rádiós fogó 200mm VDE [YT-21155](https://www.szerszamsziget.hu/yato-szigetelt-radiofogo-200mm-yt-21155-3321?utm_source=arukereso&utm_medium=cpp&utm_campaign=direct_link)

- Jokari Super 4 plus [blankoló fogó](https://aqua.hu/barkacsgepek-kezi-szerszamok/jokari-automatikus-vezetekcsupaszolo-kabelcsupaszolo-blankolo-02---60-mm2-kersztmetszetig-jokari-super-4-plus-20050-t959794) 0.2-6.0 mm2

### Mérőműszerek

- Uni-T [UT210E lakatfogó](https://www.hestore.hu/prod_10036008.html) (lakatfogó és multiméter egyben)

- (opcionális) Uni-T [UT-18 feszültségteszter](https://www.hestore.hu/prod_10038111.html) (egy kézzel tudod használni, fí relé teszter, terheléssel is tudsz feszültséget mérni, csipog, villog, gyors, szakadáskeresőnek ne használd mert arra szarrr)

